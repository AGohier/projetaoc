# Projet AOC : Simulation de capteur

Auteurs : Damien Vansteene et Arnaud Gohier

## Description
Ce projet contient le code réalisé durant le module d'AOC, ainsi que les documents finaux pour le rendu

## Rendu

### Rapport
Un rapport au format pdf est disponible à l'adresse suivante:
https://gitlab.com/AGohier/projetaoc/blob/master/fichiersRapport/AOC_CR_VANSTEENE_GOHIER.pdf

### Exécutable pour les tests
Un jar executable est disponible à l'adresse suivante:
https://gitlab.com/AGohier/projetaoc/blob/master/fichiersRapport/AOC_VANSTEENE_GOHIER.jar

Pour lancer ce programme, il faut utiliser la commande suivante:

```
java -jar ./AOC_VANSTEENE_GOHIER.jar
```

## Crédit
Dévelopement: Arnaud Gohier
Documentation et compte-rendu: Damien Vansteene