\documentclass[a4paper,11pt]{article}

\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{microtype}
\usepackage{hyperref}
\usepackage{framed}

\usepackage{graphicx}

\usepackage[margin=2cm]{geometry}
\setlength{\parskip}{1.5mm}
\hypersetup{
  colorlinks = true,
  linkcolor  = black
}

\title{Simulateur de capteur}
\author{Damien VANSTEENE \and Arnaud GOHIER}
\date{}

\begin{document}

\maketitle


\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}
Dans le cadre du module d'AOC, nous avons réalisé une simulation
minimaliste. Le but ici est de simuler le fonctionnement d'un
capteur et la façon dont il envoie ses données de manière asynchrone.

Après une brève présentation du fonctionnement de cette simulation,
nous reviendrons en détail sur la conception de ce programme.
Nous nous pencherons plus particulièrement sur l'utilisation de patrons
de conception bien connu,
afin d'identifier leur utilité dans le cadre de ce projet.

\section{Présentation de la simulation}
Le but premier de ce projet étant l'étude de certains patrons de
conception, l'aspect simulation de ce dernier est fortement
limité.

Le composant principal de la simulation est un capteur virtuel.
Il est implémenté par la classe \emph{Generator} qui génère ici une série
d'entiers croissante avec un délais d'attente de 20 ms.

Ces données passent ensuite dans un système de diffusion.
Ce système est un peu plus complexe. En effet, la simulation
permet de choisir entre différentes stratégies de transmition
des données aux exploitants virtuels.
Ce système de diffusion correspond à l'interface 
\emph{DiffusionInterface} et aux différentes classes qui l'implémente.

Afin de relier les exploitants virtuels et le système de diffusion,
nous avons également simulé un réseau de diffusion physique.
Le but de ce réseau est d'ajouter de la latence entre les composants
de notre simulation. C'est la classe \emph{Canal} qui gère ce
composant simulé et son fonctionnement est simple. Toutes données
reçues par un canal est envoyé au destinataire après un délai aléatoire
bien plus long que l'intervalle de génération de données de notre
capteur (entre 300 et 500 ms).

Enfin, les exploitants virtuels font du traitement avec les données
qu'ils reçoivent de notre capteur. Ici aussi, les opérations sont
minimales. En effet, nos exploitants ne sont que des interfaces
de visualisation. De plus, à chaque données reçus, ces exploitants
envoient un accusé de réception au système de diffusion, à travers
nos canaux. Les exploitants sont implémentés par la classe
\emph{Displayer}.

De plus, afin de surveiller l'état de notre simulation, notre programme
contient une interface utilisateur minimaliste (en ligne de commande).
Cette interface est réparti entre les classes \emph{Main}, qui gère le
Thread principal de la simulation et l'interaction utilisateur,
et \emph{GeneralDisplayer}, qui affiche à l'écran l'état des
exploitants durant toute la simulation.

\section{Problématique du projet}
Comme nous l'avons vu, la génération de données n'est pas complexe.
La vrai difficulté de ce projet est la simulation du fonctionnement
de la transmission de données.

En effet, un vrai capteur génère des données en continu et ne
s'arrête pas quand un exploitant est en train de traiter les données
en questions. Dans une simulation, cela signifie que notre système
doit être multi-threadé.

Cette contrainte nous oblige alors à mettre en place des patrons
de conception qui permette de récupérer l'état d'un objet de façon
non-bloquante et \emph{thread-safe}, c'est à dire sans créer de
problème de corruption ou d'interblocage, par exemple.

Notre capteur lance donc un Thread dédié à la création de données et
les différents exploitants essayent donc de lire les données
en parallèles.

Ces problèmes d'accès concurrents on déjà une solution, le patron
de conception \emph{Active Object}. Nous verrons donc les différents
composants de ce patron (et de quelques autres) dans la suite de
ce compte-rendu.

\section{Stratégie de diffusion}
Notre simulation permet
de choisir la façon dont le capteur envoie ses données aux
exploitants. Nous avons implémenté trois stratégies distinctes.

La première est la diffusion dite atomique. En mode de diffusion
atomique, chaque exploitant reçoit l'intégralité des données 
dans le même ordre. Ce mode oblige donc le capteur à arrêter
de générer des données tant qu'il n'a pas reçu d'accusé de réception
de la part de tout les exploitants.

La deuxième est la diffusion séquentielle. En mode de diffusion
séquentielle, chaque exploitant reçoit les mêmes données dans le même
ordre. Ici, le système de diffusion n'assure plus aux exploitants
qu'ils reçoivent toutes les données et donc la génération continue
sans pause. Par contre, le système de diffusion doit vérifier qu'il
envoie bien les mêmes données aux exploitants.

La dernière est la diffusion causale. En mode de diffusion causale,
le système de diffusion ne garanti plus ni l'ordre ni l'exhaustivité
des données reçues par les exploitants. Nous pouvons rapprocher
ce mode de fonctionnement à celui d'UDP par exemple.

Nous voulons donc pouvoir changer de stratégie de diffusion
sans avoir à créer une simulation par stratégie. Ce comportement
est souhaité dans beaucoup de programme et un patron de conception
a donc été créer pour résoudre cette problématique: 
\emph{Strategy}.

Ce patron repose sur l'utilisation d'une interface qui expose des
méthodes communes à chaque implémentation. Cela permet de changer
d'implémentation durant l'exécution du programme. De plus, cela
simplifie drastiquement la charge de travail nécessaire
à l'ajout d'une nouvelle implémentation.

Notre application de ce patron est modélisé dans la figure
\ref{UMLStrat}.

\begin{figure}[!htbp]
\begin{framed}
\includegraphics[width=\textwidth]{strategy.png}

\end{framed}
\caption{Patron de conception Strategy dans notre simulation}
\label{UMLStrat}
\end{figure}

\section{Active Object}

Comme nous l'avons vu précédemment, la réelle difficulté de ce projet
est d'accéder aux données de notre capteur virtuel. En effet,
ce dernier génère des données en continu à partir de son Thread
dédié. Afin de régler ce problème, nous allons appliquer le
patron de conception Active Object à notre architecture.

Active Object a pour but d'utiliser de manière asynchrone un objet
(appelé servant)
depuis un autre Thread que le sien. Pour ce faire, nous devons
commencer par définir les méthodes accessibles depuis l'extérieur
de l'objet et les implémenter puis mettre en place un système
de proxy qui permettra d'appeler ces méthodes. Afin de renvoyer le
résultat de l'exécution parallèle, nous devons introduire un
objet servant de callback. De plus, chaque appel de méthode passe
par un superviseur (\emph{scheduler}) qui se charge de choisir l'ordre
d'exécution.

Notre programme est écrit en Java. Ce langage fourni dans sa
bibliothèque standard certains éléments utile pour ce patron
de conception. Le \emph{SchedulerExecutorService} (scheduler) execute
les callables qui lui ont été soumis. A l'issue de cette exécution, le
\emph{Future} préalablement retourné par le scheduler lors de la soumission du callable
est renseigné avec la valeur retournée par la methode
du servant qui a été appelée. Ces objets prééxistants nous permettent
de ne pas avoir à créer notre propre implémentation de ses éléments.
Un exemple de l'utilisation de Future est présent en figure \ref{DiagSeqFuture}.
Il ilulustre la maière dont le Scheduler remplie le Future à la fin
 de l'exécution du Callable. Cet exemple est directement tiré
de l'architecture de notre simulation.

\begin{figure}[!htbp]
  \begin{framed}
    \includegraphics[width=\textwidth]{diffuseur3.png}

  \end{framed}
  \caption{Diagramme de séquence, exemple de remplissage du Future}
  \label{DiagSeqFuture}
\end{figure}

\subsection{Lien capteur/système de diffusion}
Notre capteur génère donc des données en continu sur son Thread.
Afin de diffuser ces données, nous devons trouver un moyen
de faire savoir au système de diffusion qu'une mise à jour à eu lieu.
Nous avons ici choisi
une approche simple: inclure un appel à une fonction du système
de diffusion à chaque mise à jour de l'état du capteur.
Cette approche nous permet de considérer le groupe capteur/diffusion
comme un seul élément d'un point de vue asynchrone et
simplifie l'architecture de notre programme. Nous appellerons cet
élément \emph{générateur} pour la suite de ce compte-rendu.

\subsection{Clients Active Object}
Un client Active Object est un objet souhaitant appeler une
méthode d'un servant. Notre architecture contient deux clients.

En effet, le générateur est client des exploitants car certaines
stratégies de diffusion requiert la réception d'un accusé de la
part des exploitants pour fonctionner correctement. De plus,
les exploitants ont besoin d'accéder aux données du générateur
pour faire leurs traitements, ils sont donc client du générateur.

Cet échange d'information à double sens complexifie légèrement
notre simulation et permet de mettre en évidence le fonctionnement
du proxy Active Object.

\subsection{Canaux, proxy Active Pbject}
Notre simulation, nous l'avons vu, contient une version virtuel
d'un canal de communication. Ce canal correspond au proxy du patron
Active Object. Nous étudierons son architecture en commençant
par nous intéresser à la séquence d'actions que déclenche le générateur
à chaque mise à jour.
La figure \ref{DiagSeqDiff1} illustre cette séquence.

\begin{figure}[!htbp]
\begin{framed}
\includegraphics[width=\textwidth]{diffuseur1.png}

\end{framed}
\caption{Diagramme de séquence, nouvelle donnée du capteur}
\label{DiagSeqDiff1}
\end{figure}

Nous voyons ici que le générateur envoie un signal de mise à jour
au canal et que ce dernier réagi en créant un appel à une fonction
asynchrone. Cette fonction est finalement passée au superviseur
qui l'exécutera en temps voulu. Ce superviseur renvoie immédiatement un
objet de callback qui contiendra l'accusé de réception en
provenance de l'exploitant.
La séquence générant cet accusé de réception est présente en
figure \ref{DiagSeqDiff2}.

\begin{figure}[!htbp]
\begin{framed}
\includegraphics[width=\textwidth]{diffuseur2.png}

\end{framed}
\caption{Diagramme de séquence, création de l'accusé de reception}
\label{DiagSeqDiff2}
\end{figure}

En résumé, le générateur créé une nouvelle valeur et déclenche
l'appel à la fonction de mise à jour du proxy. Ce dernier crée
un appel à la fonction de mise à jour de son exploitant et le passe
au superviseur pour qu'elle soit exécuté dès que possible. Le
superviseur renvoi un callback qui permettra de prévenir le générateur
quand l'exploitant enverra un accusé de réception. Une fois cet accusé
reçu, le générateur peut continuer son exécution (toujours défini
par sa stratégie de diffusion). On voit très bien ici que le générateur
est un client qui appel une fonction des exploitants.

En miroir, les exploitants sont également client du générateur.
En effet, la première action que réalise un exploitant à la réception
du signal de mise à jour est de demander au générateur la nouvelle
valeur émise. La séquence déclenché par cette action est présente en
figure \ref{DiagSeqDisp1}.

\begin{figure}[!htbp]
\begin{framed}
\includegraphics[width=\textwidth]{displayer1.png}

\end{framed}
\caption{Diagramme de séquence, demande de valeur du générateur}
\label{DiagSeqDisp1}
\end{figure}

Cette séquence est similaire à celle présente en figure 
\ref{DiagSeqDiff1}. L'exploitant appelle une fonction du proxy qui
se charge de créer un appel à la fonction du générateur et de le
transmettre au superviseur. De la même façon, ce dernier prévois
l'exécution de cet appel et renvoie immédiatement un callback qui
contiendra la valeur renvoyé par le capteur. La récupération de cette
valeur est présente en figure \ref{DiagSeqDisp2}.

\begin{figure}[!htbp]
\begin{framed}
\includegraphics[width=\textwidth]{displayer2.png}

\end{framed}
\caption{Diagramme de séquence, envoie de la valeur}
\label{DiagSeqDisp2}
\end{figure}

Une fois que le callback contient bien la valeur, l'exploitant peut
continuer son travail et envoyer un accusé de réception (qui sera
intégré dans le callback de la figure \ref{DiagSeqDiff1}).

Avec cette dernière opération, notre simulation est complète.
Nous allons donc revenir rapidement sur les avantages de cette
architecture.

\section{Architecture globale}
Nous avons vu à travers les différents diagrammes de séquences
comment notre simulation fait communiquer ses composants.
Nous tenons tout de même à insister sur un point important de cette
architecture.

En effet, l'utilisation d'Active Object nous a permis de créer
une simulation réaliste du fonctionnement d'un capteur. Le générateur
fonctionne sur un Thread séparé et l'intégralité du traitement des
exploitants est asynchrone. Cela garanti un certain réalisme car
le générateur ne s'arrête pas quand les exploitants traitent les
données.

L'architecture globale de notre simulation est présente en
figure \ref{FullDiag}.


\begin{figure}[!htbp]
\begin{framed}
\includegraphics[width=\textwidth ]{a.png}

\end{framed}
\caption{Modélisation complète de notre simulation}
\label{FullDiag}
\end{figure}

\section*{Conclusion}
\addcontentsline{toc}{section}{Conclusion}

A la fin de ce projet, nous pouvons affirmer avoir compris
l'intérêt des différents patrons de conception évoqué en cours
et utilisé ici. L'utilisation de méthode connue est toujours
efficace pour résoudre un problème de conception et le patron
Active Object est très bien adapté aux architectures parallèles.

De plus, ce projet nous aura appris à implémenter
ces patrons dans le langage Java. Bien que cela ne semble pas
très important, il est toujours intéressant d'élargir son bagage
technique.

\end{document}
