package fr.istic.canal;

import fr.istic.diffusion.DiffusionInterface;
import fr.istic.displayer.imp.Displayer;

import java.util.UUID;
import java.util.concurrent.*;

/**
 * @author Arnaud Gohier
 * @author Damien Vansteene
 *
 * This is our Active Object proxy
 * This class is the link between the data display (Displayer)
 * and the data source (Diffusion)
 *
 * Since some diffusion strategy need updates on the display state,
 * this link in bi-directionnal and this class is a double proxy
 */
public class Canal {

    private Displayer displayer;
    private ScheduledExecutorService executorService;
    private DiffusionInterface diffusion;

    /**
     * Canal constructor
     * @param displayer the displayer this Canal will relay update notifications and values to
     * @param executorService the Active Object scheduler used to ensure asynchronous executions of callables
     */
    public Canal(Displayer displayer, ScheduledExecutorService executorService) {

        this.displayer = displayer;
        this.executorService = executorService;
    }

    /**
     * displayer setter
     * @param displayer new Displayer
     */
    public void setDisplayer(Displayer displayer) {
        this.displayer = displayer;
    }

    /**
     * Schedule a get value from Diffusion
     * @return Future the Active Object Promise witch will contain the value
     */
    public Future<Integer> getValue() {
        // récupère la valeur auprès de la diffusion
        Future<Integer> toReturn;
        // schedule getValue on diffusion
        Callable<Integer> callable = () -> this.diffusion.getValue(this);
        // toReturn = executorService.submit(callable);
        toReturn = executorService.schedule(callable,randDelay(), TimeUnit.MILLISECONDS);
        return toReturn;
    }

    /**
     * Schedule a update from Displayer
     * @param diffusion the Diffusion who want this update
     * @return Future the Active Object Promise witch will contain a Void object on completed update
     */
    public Future<Void> update (DiffusionInterface diffusion) {
        this.diffusion=diffusion;
        Future<Void> toReturn;
        // schedule displayer update
        Callable<Void> callable = () -> this.displayer.update(this);
        // toReturn = executorService.submit(callable);
        toReturn = executorService.schedule(callable,randDelay(), TimeUnit.MILLISECONDS);
        return toReturn;
    }

    /**
     * Generate a random Float to be used as transmission delay in the Canal
     * @return random Float
     */
    private int randDelay(){
        return ThreadLocalRandom.current().nextInt(200,500+1);
    }

}
