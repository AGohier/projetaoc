package fr.istic.generator;

import fr.istic.canal.Canal;

/**
 * @author Arnaud Gohier
 * @author Damien Vansteene
 *
 * Generator interface
 */
public interface GeneratorInterface {

    /**
     * Generate an new value and store it inside the generator
     */
    void generate();

    /**
     * Register a canal
     * A registered canal will be able to receive generated values
     * @param canal Canal we want to register
     */
    void registerCanal(Canal canal);
}
