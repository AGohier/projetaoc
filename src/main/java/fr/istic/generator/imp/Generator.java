package fr.istic.generator.imp;

import fr.istic.canal.Canal;
import fr.istic.diffusion.DiffusionInterface;
import fr.istic.generator.AsyncGeneratorInterface;
import fr.istic.generator.GeneratorInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Arnaud Gohier
 * @author Damien Vansteene
 *
 * Generator implementation
 * Implements an asynchronous generator
 */
public class Generator implements GeneratorInterface, AsyncGeneratorInterface, Runnable {

    private boolean doStop;

    private int value;
    private List<Canal> registredCanal;
    private DiffusionInterface diffusion;

    public  Generator () {
        this.registredCanal= new ArrayList<>();
    }

    public void generate() {
        this.value = (this.value +1) % Integer.MAX_VALUE;
        //this.value = new Float(Math.random()*100).intValue();
        // System.out.println(this.value);
    }

    public void registerCanal(Canal canal) {
        this.registredCanal.add(canal);
    }

    public void unRegisterCanal(Canal canal) {
        this.registredCanal.remove(canal);
    }

    public Integer getValue () {
        return new Integer(this.value);
    }

    public void setDiffusion (DiffusionInterface diffusion) {
        this.diffusion = diffusion;
    }

    public void update () {
        diffusion.diffuseValue(this.registredCanal,new Integer(this.value));
    }

    public void doStop() {
        this.doStop=true;
    }

    public void run() {
        this.doStop = false;
        while(!doStop) {
            generate();
            update();

            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
