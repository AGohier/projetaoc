package fr.istic.generator;

/**
 * @author Arnaud Gohier
 * @author Damien Vansteene
 *
 * Asynchronous generator interface
 * Strategy design pattern role : context
 */
public interface AsyncGeneratorInterface {

    /**
     * Return the current value of the generator
     * @return Current value as an Integer Object
     */
    Integer getValue();

    /**
     * Invoke the strategy to use when the generator update its value
     */
    void update();
}
