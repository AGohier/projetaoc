package fr.istic.diffusion.imp;

import fr.istic.canal.Canal;
import fr.istic.diffusion.DiffusionInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @author Arnaud Gohier
 * @author Damien Vansteene
 *
 * One of our diffusion strategy
 * This strategy diffuse a value and then block
 * the value generation until all the receiver sychronised on it
 */
public class DiffusionAtomique implements DiffusionInterface {

    private List<Future<Void>> futureList;
    private Integer value;

    /**
     * Constructor
     */
    public DiffusionAtomique() {
        this.futureList = new ArrayList<Future<Void>>();
    }

    /**
     * Implements the diffusion strategy
     *
     * This one diffuse the value and wait for all the receiver
     * to send confirmation
     * @see DiffusionInterface
     * @param canalList list of Canal (Active Object proxy)
     * @param value value to save and send
     */
    public void diffuseValue(List<Canal> canalList, Integer value) {
        this.value=value;
        // diffuse the value and wait for all Future to return
        // envoi de l'update au canal pour soumission au scheduler
        for(Canal canal: canalList){
            Future<Void> future = canal.update(this);
            if(null!=future) {
                futureList.add(future);
            }
        }
        // attente de l'ensemble des retours des futures d'update ( tous les diffuseurs ont terminé leur récupération
        // de valeure)
        for(Future<Void> future: futureList){
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        futureList.clear();
        // on rend la main au run du générateur
    }

    public Integer getValue(Canal canal) {
        return this.value;
    }
}
