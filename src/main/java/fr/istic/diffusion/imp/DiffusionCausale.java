package fr.istic.diffusion.imp;

import fr.istic.canal.Canal;
import fr.istic.diffusion.DiffusionInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * @author Arnaud Gohier
 * @author Damien Vansteene
 *
 * One of our diffusion strategy
 * This strategy diffuse a value and do not wait
 * before diffusing the next one
 * It does not block the generation either
 */
public class DiffusionCausale implements DiffusionInterface {
    private int value;

    /**
     * Constructor
     */
    public DiffusionCausale() { }

    /**
     * Implements the diffusion strategy
     * <p>
     * This one diffuse the value and immediately finish
     *
     * @param canalList list of Canal (Active Object proxy)
     * @param value     value to save and send
     * @see DiffusionInterface
     */
    public synchronized void diffuseValue(List<Canal> canalList, Integer value) {
        // diffuse the value wait for the last Future before accepting next value
        this.value = value;
        for(Canal canal : canalList){
            canal.update(this);
        }
    }

    public synchronized Integer getValue(Canal canal) { return value;}
}
