package fr.istic.diffusion.imp;

import fr.istic.canal.Canal;
import fr.istic.diffusion.DiffusionInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Arnaud Gohier
 * @author Damien Vansteene
 *
 * One of our diffusion strategy
 *
 * This strategy diffuse a value to all the Canal
 * and then wait for the canals to respond before
 * diffusing the next one.
 * It does not halt the generation.
 */
public class DiffusionSequencial implements DiffusionInterface {
    private int[] values;
    private int count;
    private List<Canal> canalList;
    private List<Canal> canalUpdated;

    /**
     * Constructor
     */
    public DiffusionSequencial(){
        this.count=0;
        this.values=new int[2];
        this.canalUpdated=new ArrayList<Canal>();
    }

    /**
     * Implements the diffusion strategy
     *
     * This one diffuse the value and make sure that every Canal get the same values
     * @param canalList list of Canal (Active Object proxy)
     * @param value value to save and send
     */
    public synchronized void diffuseValue(List<Canal> canalList, Integer value) {
        // diffuse the value wait for the last Future before accepting next value
        this.values[0]=value;
        this.canalList=canalList;
        triggerDiffusion();// initialisation
    }

    /**
     * Return the value wanted by the Canal
     * and remove the Canal from the list
     * of up-to-date Canal
     * @param canal Canal wanting the value
     * @return
     */
    public synchronized Integer getValue(Canal canal) {
        if(this.canalUpdated.contains(canal)){
            this.canalUpdated.remove(canal);
            this.count--;
        }
        //System.out.println("count = "+this.count+" -");
        int toReturn = this.values[1];
        triggerDiffusion(); // as soon as the last call of get value came back -> new diffusion
        return toReturn;

    }

    /**
     * Private function used by diffuseValue
     * Concrete diffusion
     */
    private void triggerDiffusion(){
        if(0==this.count){
            this.values[1]=this.values[0];
            for(Canal canal: canalList){
                canal.update(this);
                this.canalUpdated.add(canal);
                this.count++;
                //System.out.println("count = "+this.count+" +");
            }
        }
    }

}
