package fr.istic.diffusion;

import fr.istic.canal.Canal;

import java.util.List;

/**
 * @author Arnaud Gohier
 * @author Damien Vansteene
 *
 * Diffusion Interface
 * Strategy design pattern role : strategy
 *
 * This is also (to an extend) a client from an Active Object pattern
 */
public interface DiffusionInterface {

    /**
     * This function does 2 things
     *
     * Pass te value down to our strategy
     *
     * Diffuse the value to a list of proxy
     * according to the specified strategy
     * @param canalList list of Canal (Active Object proxy)
     * @param value value to save and send
     */
    void diffuseValue(List<Canal> canalList, Integer value);

    /**
     * Return the received value from generator
     * according to the specified strategy
     * @param canal Canal wanting the value
     * @return value saved into our strategy
     */
    Integer getValue(Canal canal);
}
