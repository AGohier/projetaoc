package fr.istic;

import fr.istic.canal.Canal;
import fr.istic.diffusion.imp.DiffusionAtomique;
import fr.istic.diffusion.imp.DiffusionCausale;
import fr.istic.diffusion.imp.DiffusionSequencial;
import fr.istic.displayer.GeneralDisplayer;
import fr.istic.displayer.imp.Displayer;
import fr.istic.generator.imp.Generator;

import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * @author Arnaud Gohier
 * @author Damien Vansteene
 *
 * Main class of our application
 * Manage user interaction
 */
public class Main {
    public  static ScheduledExecutorService executorService;

    private static Generator generator;

    private static GeneralDisplayer generalDisplayer;

    private static Displayer displayer1;
    private static Displayer displayer2;
    private static Displayer displayer3;
    private static Displayer displayer4;

    public static void main(String[] args) {

        // Execution variable

        Boolean execution = true;

        // Objects





        // scheduler construct
        executorService = Executors.newScheduledThreadPool(500);

        // generator construct
        //generator = new Generator();

        // displayers construct


        // canal setup
        //Canal canal1 = new Canal(displayer1, executorService);
        //Canal canal2 = new Canal(displayer2, executorService);
        //Canal canal3 = new Canal(displayer3, executorService);
        //Canal canal4 = new Canal(displayer4, executorService);

        //generator.registerCanal(canal1);
        //generator.registerCanal(canal2);
        //generator.registerCanal(canal3);
        //generator.registerCanal(canal4);

        while(execution) {

            Thread generatorThread;
            Thread displayThread;
            String waitingInput;

            System.out.println("Enter your option: \n" +
                                "[1] Diffusion Atomique\n"+
                                "[2] Diffusion Causale\n"+
                                "[3] Diffusion Sequencial\n"+
                                "[out] Exit");
            Scanner scanner = new Scanner(System.in);
            String option = scanner.nextLine();
            switch (option) {
                case "1" :
                    // lancement du générateur en diffusion atomique
                    System.out.println("Diffusion Atomique\n");
                    generator = initGen();
                    generalDisplayer.initialize();
                    generator.setDiffusion(new DiffusionAtomique());
                    generatorThread = new Thread(generator);
                    generatorThread.start();


                    // arrêt de la génération
                    waitingInput = scanner.nextLine(); // point d'arrêt jusqu'à Enter dans la console
                    generator.doStop();
                    try { Thread.sleep(1000); } catch (InterruptedException e) { e.printStackTrace(); }
                    generalDisplayer.displayReport();
                    generalDisplayer.displayAtomicityReport();
                    break;
                case "2" :
                    // lancement du générateur en diffusion causale
                    System.out.println("Diffusion Causale\n");
                    generator = initGen();
                    generalDisplayer.initialize();
                    generator.setDiffusion(new DiffusionCausale());
                    generatorThread = new Thread(generator);
                    generatorThread.start();

                    // arrêt de la génération
                    waitingInput = scanner.nextLine(); // point d'arrêt jusqu'à Enter dans la console
                    generator.doStop();
                    try { Thread.sleep(1000); } catch (InterruptedException e) { e.printStackTrace(); }
                    generalDisplayer.displayReport();
                    break;
                case "3" :
                    // lancement du générateur en diffusion sequentielle
                    System.out.println("Diffusion Sequensial\n");
                    generator = initGen();
                    generalDisplayer.initialize();
                    generator.setDiffusion(new DiffusionSequencial());
                    generatorThread = new Thread(generator);
                    generatorThread.start();

                    // arrêt de la génération
                    waitingInput = scanner.nextLine(); // point d'arrêt jusqu'à Enter dans la console
                    generator.doStop();
                    try { Thread.sleep(1000); } catch (InterruptedException e) { e.printStackTrace(); }
                    generalDisplayer.displayReport();
                    break;
                case "out":
                    System.out.println("Exit, have a nice day!\n");
                    execution=false;
                    System.exit(0);
                    break;
                default:
                    break;
            }

        }




    }

    private static Generator initGen(){
        Generator generator = new Generator();

        //reset Displayer
        displayer1 = new Displayer("Disp1");
        displayer2 = new Displayer("Disp2");
        displayer3 = new Displayer("Disp3");
        displayer4 = new Displayer("Disp4");
        generalDisplayer = new GeneralDisplayer();
        displayer1.setGeneralDisplayer(generalDisplayer,1);
        displayer2.setGeneralDisplayer(generalDisplayer,2);
        displayer3.setGeneralDisplayer(generalDisplayer,3);
        displayer4.setGeneralDisplayer(generalDisplayer,4);

        // reset Canal
        Canal canal1 = new Canal(displayer1, executorService);
        Canal canal2 = new Canal(displayer2, executorService);
        Canal canal3 = new Canal(displayer3, executorService);
        Canal canal4 = new Canal(displayer4, executorService);

        //reset generator
        generator.registerCanal(canal1);
        generator.registerCanal(canal2);
        generator.registerCanal(canal3);
        generator.registerCanal(canal4);

        return generator;
    }
}
