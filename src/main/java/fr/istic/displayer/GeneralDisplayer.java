package fr.istic.displayer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Arnaud Gohier
 * @author Damien Vansteene
 *
 * Main UI
 */
public class GeneralDisplayer {

    private int value1;
    private int value2;
    private int value3;
    private int value4;
    private List<Integer> values1;
    private List<Integer> values2;
    private List<Integer> values3;
    private List<Integer> values4;
    private String display;
    private boolean atomicity;
    private int atomicityCount;

    public GeneralDisplayer(){
        this.value1=-1;
        this.value2=-1;
        this.value3=-1;
        this.value4=-1;
        this.display="";
        this.values1 = new ArrayList<Integer>();
        this.values2 = new ArrayList<Integer>();
        this.values3 = new ArrayList<Integer>();
        this.values4 = new ArrayList<Integer>();
        this.atomicity=true;
        this.atomicityCount=0;
    }

    public synchronized void setValue(int canal,int val) {
        switch (canal){
            case 1:
                if(val!=this.value1) {
                    this.value1 = val;
                    this.values1.add(val);
                }
                break;
            case 2:
                if(val!=this.value2) {
                    this.value2 = val;
                    this.values2.add(val);
                }
                break;
            case 3:
                if(val!=this.value3) {
                    this.value3 = val;
                    this.values3.add(val);
                }
                break;
            case 4:
                if(val!=this.value4) {
                    this.value4 = val;
                    this.values4.add(val);
                }
                break;
        }

        atomicityCount++;
        if(4==atomicityCount){
            atomicityCount=0;
            if(
                    (this.value1!=this.value2) ||
                    (this.value1!=this.value3) ||
                    (this.value1!=this.value4)
            ){
                this.atomicity=false;
            }
        }

        String temp = "Disp1: "+this.value1+", Disp2: "+this.value2+", Disp3: "+this.value3+", Disp4: "+this.value4;//+hilightDiff(this.value1,this.value2,this.value3,this.value4);
        // if(!atomicity){temp+=" <----";}
        if(!this.display.equals(temp)){
            this.display=temp;
            System.out.println(display);
            // System.out.println(this.atomicityCount);
        }


    }

    public void initialize(){
        this.value1=-1;
        this.value2=-1;
        this.value3=-1;
        this.value4=-1;
        this.display="";
        this.values1.clear();
        this.values2.clear();
        this.values3.clear();
        this.values4.clear();
        this.atomicity=true;
        this.atomicityCount=0;
    }

    public void displayReport(){
        int minSize;
        boolean continueParse=true;
        minSize=this.values1.size();
        if(this.values2.size()<minSize){minSize=this.values2.size();}
        if(this.values3.size()<minSize){minSize=this.values3.size();}
        if(this.values4.size()<minSize){minSize=this.values4.size();}

        String toPrint = "\nReport:\nLes displayers ont pris "+minSize+" valeurs:\n[";
        for(int i=0;continueParse&&(i<minSize);i++){
            if(
                    !this.values1.get(i).equals(this.values2.get(i)) ||
                    !this.values1.get(i).equals(this.values3.get(i)) ||
                    !this.values1.get(i).equals(this.values4.get(i))
            ){
                continueParse=true;
                toPrint+="*Valeure: "+(i+1)+" *"+this.values1.get(i)+"-"+this.values2.get(i)+"-"+this.values3.get(i)+"-"+this.values4.get(i)+"*\n";
            }else{
                toPrint+=this.values1.get(i)+", ";
            }
        }
        toPrint+="*]";
        System.out.println(toPrint);
        if(continueParse){
            System.out.println("Série coherante\n");
        }else{
            System.out.println("Différance de séquence des displayers\n");
        }
    }

    public void displayAtomicityReport(){
        System.out.println("Diffusion atomique respectée? "+this.atomicity+"\n");
    }

/*
    private String hilightDiff(int a, int b, int c, int d) {
        String toReturn = "";
        if(a != b){
            if((c != a)&(c != b)){
                toReturn = " <------------ 1";
            }else if((d != a)&&(d != b)){
                toReturn = " <------------ 2";
            }
        }else{// a = b
            if((a != c)&&(a != d)&&(c != d)){
                toReturn = " <------------ 3";
            }
        }
        return toReturn;
    }
*/
}
