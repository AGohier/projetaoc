package fr.istic.displayer;

import fr.istic.canal.Canal;

/**
 * @author Arnaud Gohier
 * @author Damien Vansteene
 *
 * Asynchronous Displayer Interface
 * Part of the Active Object pattern
 */
public interface AsyncDisplayerInterface {
    /**
     * Update the Displayer
     * with data for the Canal passed in parameter
     * @param canal Datasource for the update
     * @return Void object to send notification to the Canal
     */
    Void update(Canal canal);
}
