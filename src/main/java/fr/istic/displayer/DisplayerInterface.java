package fr.istic.displayer;

/**
 * @author Arnaud Gohier
 * @author Damien Vansteene
 *
 * Displayer interface
 */
public interface DisplayerInterface {

    /**
     * Get the current value of the displayer
     * @return current value as primitive int
     */
    int display();
}
