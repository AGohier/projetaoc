package fr.istic.displayer.imp;

import fr.istic.canal.Canal;
import fr.istic.displayer.AsyncDisplayerInterface;
import fr.istic.displayer.DisplayerInterface;
import fr.istic.displayer.GeneralDisplayer;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @author Arnaud Gohier
 * @author Damien Vansteene
 *
 * Displayer implementation
 */
public class Displayer implements AsyncDisplayerInterface, DisplayerInterface {

    private int value;
    private String name;
    private GeneralDisplayer generalDisplayer;
    private int generalDisplayerCanal;

    /**
     * Constructor
     * @param name Friendly identifier
     */
    public Displayer(String name){
        this.value = -1;
        this.name = name;
    }

    /**
     * Setter for the user interface
     * @param generalDisplayer UI
     * @param canal UI parameter
     */
    public void setGeneralDisplayer(GeneralDisplayer generalDisplayer,int canal){
        this.generalDisplayer=generalDisplayer;
        this.generalDisplayerCanal= canal;
    }


    public Void update(Canal canal) {
        // on utilise le getValue async pour récupérer un Fututre<Integer>
        Future<Integer> futureValue;
        // ask for the value
        futureValue=canal.getValue();

        try {
            // attente et récupération de la valeure avant de rendre la main ( et l'écriture du Future<Void> à la diffusion)
            this.value = futureValue.get();
            this.generalDisplayer.setValue(this.generalDisplayerCanal,this.value);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    public int display() {
        return this.value;
    }

    /**
     * Getter
     * @return Friendly identifier
     */
    public String getName(){return this.name;}

}
